FROM alpine:3.11

ENV GLIBC_VER=2.31-r0

RUN apk --no-cache add \
        binutils \
        curl \
    # Installing the dependencies for glibc - needed since not available natively with alpine.
    && curl -sL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-${GLIBC_VER}.apk \
    && curl -sLO https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VER}/glibc-bin-${GLIBC_VER}.apk \
    # Installing glibc with apk
    && apk add --no-cache \
        glibc-${GLIBC_VER}.apk \
        glibc-bin-${GLIBC_VER}.apk \
    # Now, downloading and unzipping aws cli as normal
    && curl -sL https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip \
    && unzip awscliv2.zip \
    && aws/install \
    # Now, removing unecessary cache files to reduce image size
    && rm -rf \
        awscliv2.zip \
        aws \
        /usr/local/aws-cli/v2/*/dist/aws_completer \
        /usr/local/aws-cli/v2/*/dist/awscli/data/ac.index \
        /usr/local/aws-cli/v2/*/dist/awscli/examples \
    # Removing installed glibc depedencies
    && rm glibc-${GLIBC_VER}.apk \
    && rm glibc-bin-${GLIBC_VER}.apk \
    && rm -rf /var/cache/apk/* \
    # Removing installed commands
    && apk --no-cache del \
        binutils \
        curl

# Adding docker
RUN apk add docker
# Adding bash
RUN apk add bash
RUN aws --version && docker --version

CMD ["/bin/bash"]